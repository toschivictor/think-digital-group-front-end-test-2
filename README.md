# Masterclass Channel

> A Vue.js app using vue-cli for project scaffolding.

The base app was created, it's consuming the API and some components where created with unit tests.

## Introduction

It's a Single Page Application which consumes an API and display its data.

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report

# run unit tests
npm run unit

# run all tests
npm test
```

For a detailed explanation on how things work, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).
