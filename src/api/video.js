import axios from 'axios'

const videosEndpoint = 'https://titan.asset.tv/api/channel-view-json/2240'

export default {
  getVideos () {
    return axios.get(videosEndpoint)
  }
}
