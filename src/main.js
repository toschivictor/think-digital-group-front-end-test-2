import Vue from 'vue'
import App from './App'

Vue.config.productionTip = false

/* eslint-disable no-new */
/**
 * Instatiate Vue.js
 * @constructor
 **/
new Vue({
  el: '#app',
  template: '<App/>',
  components: { App }
})
