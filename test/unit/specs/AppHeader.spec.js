import Vue from 'vue'
import AppHeader from '@/components/AppHeader'

describe('HelloWorld.vue', () => {
  it('should render the logo image', () => {
    const Constructor = Vue.extend(AppHeader)
    const vm = new Constructor().$mount()
    expect(typeof vm.$el.querySelector('img'))
    .toEqual('object')
  })
})
