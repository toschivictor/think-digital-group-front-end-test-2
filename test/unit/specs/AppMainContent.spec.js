import Vue from 'vue'
import AppMainContent from '@/components/AppMainContent'

describe('HelloWorld.vue', () => {
  it('should render the default title text', () => {
    const Constructor = Vue.extend(AppMainContent)
    const vm = new Constructor().$mount()
    expect(vm.$el.querySelector('section h2').textContent)
    .toEqual('Title')
  })
})
